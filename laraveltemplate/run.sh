#/bin/env/bash

echo "Laravel Server Starting..."

set -a

. ./.env && env && npm run dev && php artisan serve --port 8000
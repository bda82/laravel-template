<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;


class DashboardController extends Controller
{
    public function dashboard(Request $request)
    {
        return view('dashboard.dashboard');
    }

    public function stats(Request $request)
    {
        return view('user.stats');
    }
}
# laravel-template

1. Install php
2. Install composer globally
3. Create Laravel Project
```sh
composer create-project laravel/laravel ProjectName
```
4. Install npm packages
```sh
npm install --no-optional
```
and laravel packages
```sh
composer install
```
If there are some projects on your OS, remove folder ```node_modules```, folder ```vendor```, file ```package-lock.json``` and ```file coposer.lock```.
5. Compile nmp resources
```sh
npm run dev
```
6. Install packages (example: Guzzle)
```sh
composer require guzzlehttp/guzzle
```
7. Install Laravel mix
```sh
npm install laravel-mix --save-dev
```
8. Install default auth with views and routes
```sh
composer require laravel/ui
```
and
```sh
php artisan ui bootstrap --auth
```
and
```sh
npm run dev
```
9. Create .env file
```sh
cp .env.example .env
```
10. In .env:
    - change APP_NAME
    - change APP_URL=http://localhost:8000
    - change DB_DATABASE=laravel
    - change DB_USERNAME=root
    - change DB_PASSWORD=password
11. Generate application key
```sh
php artisan key:generate
```
12. Create database for project in MySQL server
13. Call DB migrations
```sh
php artisan migrate
```
14. Run server
```sh
php artisan serve
```
or use my run.sh file
```sh
./run.sh
```

All View routes (for pages) are in ```routes/web.php```

All API routes (for JS calls) are in ```routes/api.php```

All Views (pages) are in ```resources/views```

All controllers for Routes are in ```app/Http/Controllers```

Main layout (nav and others) in ```resources/views/layouts/app.blade.php```
